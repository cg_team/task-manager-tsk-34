package ru.inshakov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;

import java.io.FileOutputStream;
import java.util.Optional;

public class BackupSaveCommand extends AbstractDataCommand {


    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "backup-save";
    }

    @NotNull
    @Override
    public String description() {
        return "save backup to xml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath = serviceLocator.getPropertyService().getFileXmlPath("backup");
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}

