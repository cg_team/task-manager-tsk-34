package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.Project;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, final String projectId);

    Task bindTaskById(String userId, final String taskId, final String projectId);

    Task unbindTaskById(String userId, final String taskId);

    Project removeProjectById(String userId, final String projectId);

    Project removeProjectByIndex(String userId, final Integer index);

    Project removeProjectByName(String userId, final String name);

}
