package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AuthAbstractCommand;

public class DataJsonLoadJaxBCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-load-json-j";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load data from JSON by JaxB.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataJsonJaxB(getSession());
    }

}