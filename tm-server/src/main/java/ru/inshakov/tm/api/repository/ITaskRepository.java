package ru.inshakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    Task bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    Task unbindTaskById(final String userId, final String id);

    Task findByName(final String userId, final String name);

    Task findByIndex(final String userId, final int index);

    Task removeByName(final String userId, final String name);

    Task removeByIndex(final String userId, final int index);

    @Nullable Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

}
