package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.user.AccessDeniedException;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @Nullable
    protected String userId;

}
