package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.IPropertyService;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
