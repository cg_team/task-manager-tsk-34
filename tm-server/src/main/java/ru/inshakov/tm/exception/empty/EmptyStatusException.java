package ru.inshakov.tm.exception.empty;

public final class EmptyStatusException extends RuntimeException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}
