package ru.inshakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void addAll(final Collection<E> collection);

    E add(final E entity);

    E findById(final String id);

    void clear();

    E removeById(final String id);

    E remove(final E entity);

}
