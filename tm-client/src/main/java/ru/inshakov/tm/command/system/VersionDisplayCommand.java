package ru.inshakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.inshakov.tm.command.AbstractCommand;

public class VersionDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}
