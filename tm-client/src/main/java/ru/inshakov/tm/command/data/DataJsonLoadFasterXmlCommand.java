package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AuthAbstractCommand;

public class DataJsonLoadFasterXMLCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-load-json-f";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load data from JSON by FasterXML.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataJson(getSession());
    }

}