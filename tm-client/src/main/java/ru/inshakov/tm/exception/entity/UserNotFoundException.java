package ru.inshakov.tm.exception.entity;

import ru.inshakov.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}

