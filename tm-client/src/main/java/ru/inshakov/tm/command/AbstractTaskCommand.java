package ru.inshakov.tm.command;

import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.endpoint.Task;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected static void showTask(final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus());
    }

}
