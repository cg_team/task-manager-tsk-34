package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.api.service.IBusinessService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.empty.EmptyStatusException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.*;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    private final IBusinessRepository<E> repository;

    @NotNull
    public AbstractBusinessService(@NotNull final IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public void addAll(@NotNull final String userId, @Nullable final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.addAll(userId, collection);
    }

    @Nullable
    @Override
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        return repository.add(userId, entity);
    }

    @NotNull
    @Override
    public E findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @NotNull
    @Override
    public E removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.removeById(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Nullable
    @Override
    public E remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) return null;
        return repository.remove(userId, entity);
    }

}

