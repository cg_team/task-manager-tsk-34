package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AuthAbstractCommand;

public class DataXmlLoadJaxBCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-load-xml-j";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load data from XML by JaxB.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataXmlJaxB(getSession());
    }

}