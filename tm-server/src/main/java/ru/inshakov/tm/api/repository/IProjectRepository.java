package ru.inshakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    Project removeByName(final String userId, final String name);

    Project removeByIndex(final String userId, final int index);

    @Nullable Project add(@NotNull String userId, @Nullable String name, @Nullable String description);
}
