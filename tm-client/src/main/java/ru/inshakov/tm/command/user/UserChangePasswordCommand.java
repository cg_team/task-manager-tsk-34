package ru.inshakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.empty.EmptySessionException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "change password of user profile";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(EmptySessionException::new);
        @NotNull final String userId = session.getUserId();
        System.out.println("ENTER NEW PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getSessionEndpoint().setPassword(session, password);
    }

}

