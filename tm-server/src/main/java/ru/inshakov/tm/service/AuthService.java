package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.IAuthService;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyLoginException;
import ru.inshakov.tm.exception.empty.EmptyPasswordException;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.util.HashUtil;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    @NotNull
    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        if (userId == null) throw new AccessDeniedException();
        @NotNull final User user = Optional.ofNullable(userService.findById(userId))
                .orElseThrow(AccessDeniedException::new);
        @NotNull final Role role = Optional.of(user.getRole())
                .orElseThrow(AccessDeniedException::new);
        for (@Nullable final Role item : roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login))
                .orElseThrow(AccessDeniedException::new);
        @NotNull final String hash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(AccessDeniedException::new);
        if (!hash.equals(user.getPasswordHash()) || user.isLocked()) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.add(login, password, email);
    }
}

