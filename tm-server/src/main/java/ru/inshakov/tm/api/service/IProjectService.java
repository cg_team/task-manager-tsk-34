package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IBusinessService<Project> {

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final Integer index);

    Project removeByName(final String userId, final String name);

    Project removeByIndex(final String userId, final Integer index);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project startById(final String userId, final String id);

    Project startByIndex(final String userId, final Integer index);

    Project startByName(final String userId, final String name);

    Project finishById(final String userId, final String id);

    Project finishByIndex(final String userId, final Integer index);

    Project finishByName(final String userId, final String name);

    Project add(String userId, String name, String description);
}
