package ru.inshakov.tm.command.auth;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AuthAbstractCommand;
import ru.inshakov.tm.util.TerminalUtil;

public class PasswordChangeCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "password-change";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter new password");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getSessionEndpoint().setPassword(getSession(), password);
    }
}
