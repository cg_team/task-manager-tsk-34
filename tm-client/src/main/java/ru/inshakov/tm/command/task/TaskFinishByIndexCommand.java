package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskFinishByIndexCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = serviceLocator.getTaskEndpoint().finishTaskByIndex(getSession(), index);
        if (task == null) throw new TaskNotFoundException();
    }
}
